from flask import Flask,render_template,jsonify,request,Blueprint,abort,make_response
from flask_sqlalchemy import SQLAlchemy
from .models import db,OsmChangesetDetails,AggregateTracker,Aggregate
from flask_cors import CORS
from sqlalchemy.sql import func,label
from sqlalchemy import desc

api = Blueprint('api', __name__)
CORS(api) # enable CORS on the api blueprint

                    
#campaign details
@api.route('/api/campaign_details/<string:campaign_name>',methods=['GET'])
def get_userdetails_bycampname(campaign_name):

    users_changeset_aggregate = Aggregate.query.filter_by(campaign_name=campaign_name).all()
    result_list = []
    for aggregate in users_changeset_aggregate:
        result_dict = {}
        result_dict['osm_id'] = aggregate.osm_id
        result_dict['display_name'] = aggregate.display_name
        result_dict['score'] = aggregate.total_score
        result_list.append(result_dict)
    if result_list:
        return jsonify(result_list)
    else:
        abort(404)

#userprofile details
@api.route('/api/user_details/<string:campaign_name>/<string:display_name>',methods=['GET'])
def get_userdetails(campaign_name,display_name):
    aggregate_query = Aggregate.query.filter(Aggregate.display_name==display_name,Aggregate.campaign_name==campaign_name).first()
    if aggregate_query:
        resultset = [{"osm_id":aggregate_query.osm_id,"name":aggregate_query.display_name,"campaign_name":aggregate_query.campaign_name,"score":aggregate_query.total_score}]
        return jsonify(resultset) 
    else:
        abort(404)
        
@api.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': 'Result not found'}), 404)
