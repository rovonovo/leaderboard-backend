from flask import Flask
from flask_sqlalchemy import SQLAlchemy


db = SQLAlchemy()


def create_app():
    """Construct the core application."""
    app = Flask(__name__, instance_relative_config=False)
    app.config.from_object('config.Config')

    #connect db
    db.init_app(app)


    #blue prints for api routes in our app
    from .api import api as api_blueprint
    app.register_blueprint(api_blueprint)


    with app.app_context():
        db.create_all()  # Create database tables for our data models

    return app