from . import db
from sqlalchemy.sql import func
class OsmChangesetDetails(db.Model):

    __tablename__ = 'osm_changeset_details'

    changeset_detail_id = db.Column(db.Integer, primary_key=True)
    changeset_id = db.Column(db.Integer, unique=True)
    display_name = db.Column(db.String(100))
    osm_id = db.Column(db.Integer)
    campaign_name = db.Column(db.String(75))
    create_count = db.Column(db.Integer)
    modify_count = db.Column(db.Integer)
    delete_count = db.Column(db.Integer)

    def __repr__(self):
        return '<OsmChangesetDetails %r>' % self.changeset_id
class AggregateTracker(db.Model):

    __tablename__ = 'leaderboard_aggregate_trackers'

    id = db.Column(db.Integer, primary_key=True)
    changeset_id = db.Column(
        db.Integer, db.ForeignKey('osm_changeset_details.changeset_detail_id'), nullable=False
    )
    created_at = db.Column(db.DateTime(timezone=True), server_default=func.now())
    
    def __repr__(self):
        return '<AggregateTracker %r>' % self.changeset_id

    
class Aggregate(db.Model):

    __tablename__ = 'leaderboard_aggregates'
    __table_args__ = (
        db.UniqueConstraint('campaign_name', 'osm_id'),
    )

    id = db.Column(db.Integer, primary_key=True)
    campaign_name = db.Column(db.String(80), nullable=False)
    osm_id = db.Column(db.Integer, nullable=False)
    display_name = db.Column(db.String(100))
    total_score = db.Column(db.Integer, default=0)
    updated_at = db.Column(db.DateTime(timezone=True), onupdate=func.now())
    
    def __repr__(self):
        return '<Aggregate %r>' % self.osm_id


    