# Importing requests module for sending HTTP Requests
# and receiving response data
import requests
import csv
from datetime import datetime,timedelta
import re
import subprocess
#cmd to generate tm user api token for authentication
cmd = "[path to tasking manager/venv/bin/python] [path to tasking-manager/manage.py]  gen_token  -u 10522618"
try:
    returned_output = subprocess.check_output(cmd,shell=True)
    search_token = re.search("b'(.*)'", returned_output)
    TM_USER_TOKEN = search_token.group(1)
    print(TM_USER_TOKEN)
    TM_USR_REQ_URI = "https://mapmykerala.in/api/v2/users/"
    TM_USR_TOKEN_PREFIX = 'Token '
    TM_USR_AUTH_PARAM = TM_USR_TOKEN_PREFIX + TM_USER_TOKEN
    resp = requests.get(TM_USR_REQ_URI,headers={'accept':'application/json','Authorization': TM_USR_AUTH_PARAM})
    #proceed only if the status code is 200
    status_code = resp.status_code
    if status_code == 200:
        result = resp.json()
        PAGE_NUM = result['pagination']['pages']
        userList = []
        for page in range(1,PAGE_NUM+1):
            TM_USR_REQ_URI = "https://mapmykerala.in/api/v2/users/?page=" + str(page)
            resp = requests.get(TM_USR_REQ_URI,headers={'accept':'application/json','Authorization': TM_USR_AUTH_PARAM})
            result = resp.json()
            userslist = result['users']
            for users in userslist:
                userList.append(users['username'])
        print(userList)
        with open("./userslist.csv","w") as csvfile:
            writer = csv.writer(csvfile)
            for usersname in userList:
                writer.writerow([usersname])
        print("List has been updated @",datetime.strftime(datetime.now(), '%Y-%m-%d %H:%M:%S'))
    else:
        print("script failed",status_code)
except subprocess.CalledProcessError as cmdexc:                                                                                                   
    print("error code", cmdexc.returncode, cmdexc.output)






        
