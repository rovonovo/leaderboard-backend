class Config:
    SECRET_KEY = '6f156309de4644e2a2524893b8bc0340'
    SQLALCHEMY_TRACK_MODIFICATIONS = True
    DEBUG = True
    DB_TYPE = 'postgresql+psycopg2'
    DB_USER = ''
    DB_USER_PASS = ''
    DB_URL = ''
    DB_NAME = ''
    
    SQLALCHEMY_DATABASE_URI = '{db_type}://{user}:{pw}@{url}/{db}'.format(
        db_type=DB_TYPE,
        user=DB_USER,
        pw=DB_USER_PASS,
        url=DB_URL,
        db=DB_NAME
    )
    
