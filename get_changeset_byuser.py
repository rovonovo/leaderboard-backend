# Importing requests module for sending HTTP Requests
# and receiving response data
import requests
from datetime import datetime,timedelta
import os.path
import math
import csv
#importing psycopg module for use PostgreSQL from Python
import psycopg2
import psycopg2.extras
#Connecting to the db server
connection = psycopg2.connect(user="",password="",host="",port="",database="")
#create a new cursor object by calling the cursor() method of the connection object.
cursor = connection.cursor(cursor_factory=psycopg2.extras.DictCursor)

CAMPAIGN_NAME = 'test-campaign'
IN_BBOX = "74.6406,8.096,77.4119,12.7945"
#tm userlist from csvfile
userslist =[]
with open('./userslist.csv', mode='r') as csv_file:
    csv_reader = csv.reader(csv_file)
    for rows in csv_reader:
        userslist.append(rows)
userList = [ item for elem in userslist for item in elem]    
userList = ','.join(map(str, userList))
print("Tm userlist : ",userList)
#To split the userlist into sublists of 10 elements ,if the length of userlist is greater than 10.(the no of elements in a sublist is set to be 10 based on the geometry string length)
finalUserList = []
tempList = []
tempUserString = ''
USER_COUNT_PER_REQUEST = 20
startPos = 0
endPos = 0
userList = userList.split(",")
userList = [x.strip(' ') for x in userList]
userCount = len(userList) 
maxRows = float(userCount)/USER_COUNT_PER_REQUEST
maxRows = math.ceil(maxRows)
maxRows = math.trunc(maxRows)
for i in range(maxRows):
    if (userCount - startPos) > USER_COUNT_PER_REQUEST:
            endPos = endPos + USER_COUNT_PER_REQUEST
    else:
        endPos = userCount
    
    tempList = userList[startPos:endPos]
    tempUserString = ','.join(tempList)
    finalUserList.append(tempUserString)
    startPos = endPos



#if the file exists ,then read the date from the file else set the "from date" as of 24 hr before
if os.path.isfile('/var/osm_changeset/osm_changeset_timestamp.txt'):
    #opening the file
    f = open("/var/osm_changeset/osm_changeset_timestamp.txt", "r")
    file_contents = f.read()
    FROMDATE = file_contents.strip('\n')
    #set 5 min before timestamp to get the missing changeset details during the cron run 
    FROMDATE = datetime.strptime(FROMDATE, "%Y-%m-%d %H:%M:%S")
    FROMDATE -= timedelta(minutes=5)
    FROMDATE_TOSTRING = str(FROMDATE)
    print("from-date from the file:",FROMDATE_TOSTRING)
    f.close()
else:
    FROM_DATE = datetime.strftime(datetime.now() - timedelta(1), '%Y-%m-%d %H:%M:%S')
    print("24 hr before from date:",FROM_DATE)
    #UTC timeconversion = IST - 5.30(hr)
    utctime = datetime.strptime(FROM_DATE, "%Y-%m-%d %H:%M:%S")
    utctime -= timedelta(hours=5,minutes=30)
    FROMDATE_TOSTRING = utctime.strftime("%Y-%m-%d %H:%M:%S")
    print("UTC TIME:",FROMDATE_TOSTRING)
#to save the changeset ids from the osmcha api response and then write it to the csv file
changeset_id_list = []

#open the csv file ,read the changeset ids and save it to a list
if os.path.isfile('./changeset_details.csv'):
    changesetidlist =[]
    with open('./changeset_details.csv', mode='r') as csv_file:
        csv_reader = csv.reader(csv_file)
        for row in csv_reader:
            changesetidlist.append(row)
    final_changeset_id_list = [ item for elem in changesetidlist for item in elem]
else:
	final_changeset_id_list = []

for USERS in finalUserList:
    REQUEST_URI = "https://osmcha.org/api/v1/changesets/?in_bbox=" + IN_BBOX + "&users=" + USERS +  "&date__gte=" + FROMDATE_TOSTRING
    TOKEN_PREFIX = 'Token '
    USER_TOKEN = 'cfaf3539967f8272d792b64aac894f580947224e'
    AUTH_PARAM = TOKEN_PREFIX + USER_TOKEN
    DATA_PER_PAGE = 500
    print("before_loop_request_uri",REQUEST_URI)
    # Invoke the get API for fetching data over HTTP
    # Header information is passed along with the attribute-'headers'
    resp = requests.get(REQUEST_URI,headers={'accept':'application/json','Authorization': AUTH_PARAM})
    #proceed only if the status code is 200
    status_code = resp.status_code
    if status_code == 200:
        # if flag is 1 ,then update the timestamp
        flag = 1
        print("status_code=",status_code)
        # Getting the response data by invoking the json() method
        result = resp.json()
        #to get the total count of the return records
        count = result["count"]
        print("count=",count)
        if count >0:
            #To get the page number (The pagination returns 500 objects by page)
            page_number = float(count)/DATA_PER_PAGE
            page_number = math.ceil(page_number)
            page_number = math.trunc(page_number)
            changeset_list = []
            #To get the changeset details in each page
            for page in range(1,page_number+1):
                REQUEST_URI = "https://osmcha.org/api/v1/changesets/?in_bbox=" + IN_BBOX + "&users=" + USERS + "&date__gte=" + FROMDATE_TOSTRING + "&page=" + str(page) + "&page_size=" + str(DATA_PER_PAGE)
                print("request_uri",REQUEST_URI)
                resp = requests.get(REQUEST_URI,headers={'accept':'application/json','Authorization': AUTH_PARAM})
                result = resp.json()
                features = result["features"]
                for feature in features:
                    changeset_details = []
                    changeset_id = feature["id"]
                    #Appending Changeset Id
                    changeset_id_list.append(changeset_id)
                    if str(changeset_id) not in final_changeset_id_list:
                        changeset_details.append(changeset_id)
                        properties = feature['properties']
                        #Appending uid(osm_id) User name, Created count, Modified count, Deleted count
                        changeset_details.append(properties["uid"])
                        changeset_details.append(properties["user"])
                        changeset_details.append(properties["create"])
                        changeset_details.append(properties["modify"])
                        changeset_details.append(properties["delete"])
                        changeset_details.append(CAMPAIGN_NAME)
                        changeset_list.append(changeset_details)
            insert_to_table = "INSERT INTO osm_changeset_details (changeset_id,osm_id,display_name,create_count,modify_count,delete_count,campaign_name) VALUES(%s,%s,%s,%s,%s,%s,%s)"
            if len(changeset_list) >0:
                cursor.executemany(insert_to_table, changeset_list)
            #commit the changes to the database
            connection.commit()      
                
    else:
        print("Request to changesets failed, status_code=",status_code)
        flag = 0       

if changeset_id_list:
    with open("./changeset_details.csv","w") as csvfile:
	    writer = csv.writer(csvfile)
	    for id in changeset_id_list:
		    writer.writerow([id])

if flag == 1:
    
    #fetching last inserted changeset id from tracker table
    tracker_last_changeset_query = "SELECT changeset_id FROM leaderboard_aggregate_trackers ORDER BY id DESC LIMIT 1"
    cursor.execute(tracker_last_changeset_query)
    tracker_id_result = cursor.fetchone()
    #if tracker table is not empty
    if tracker_id_result:
        tracker_last_id = tracker_id_result[0]
        #fetching last inserted id from osmchangeset tbl
        last_inserted_chageset_query = ("SELECT changeset_detail_id "
                                        "FROM osm_changeset_details "
                                        "ORDER BY changeset_detail_id DESC LIMIT 1")
        cursor.execute(last_inserted_chageset_query)
        last_changeset_id_result = cursor.fetchone()
        last_inserted_chageset_id = last_changeset_id_result['changeset_detail_id']
        #check if any new updations. if any, then check whether the contributers score is in aggregate table , 
        #if yes , then add the score to it otherwise new users aggregate details will be added to the tbl.
        #and also add the last changeset id to the tracker tbl
        if last_inserted_chageset_id > tracker_last_id:
            users_changeset_aggregate = ("SELECT osm_id,display_name,campaign_name, "
                                         "sum(create_count) as total_create_count, "
                                         "sum(modify_count) as total_modify_count, "  
                                         "sum(delete_count) as total_delete_count "
                                         "FROM osm_changeset_details "
                                         "WHERE changeset_detail_id >  %(tracker_last_id)s "
                                         "AND campaign_name = %(campaign_name)s"
                                         "GROUP BY osm_id,display_name,campaign_name")
            #params = (tracker_last_id,CAMPAIGN_NAME)
            params = {'tracker_last_id':tracker_last_id,'campaign_name':CAMPAIGN_NAME}
            cursor.execute(users_changeset_aggregate,params)
            users_aggregate_resultset = cursor.fetchall()
            for aggregates in users_aggregate_resultset:
                #check if the aggregate of a user in the campaign already in the aggregate tbl
                isexist_osmid_query = "SELECT * FROM leaderboard_aggregates WHERE osm_id= %(osm_id)s AND campaign_name = %(campaign_name)s"
            
                params = {'osm_id':aggregates['osm_id'],'campaign_name':aggregates['campaign_name']}
                cursor.execute(isexist_osmid_query,params)
                resultset = cursor.fetchone()
                if resultset:
                    #create/delete - 3points , Modify-1point
                    new_score = (aggregates['total_create_count']*3)+(aggregates['total_modify_count']*1)+(aggregates['total_delete_count']*3)
                    new_total_score = new_score + resultset['total_score']
            
                    update_query = ("UPDATE leaderboard_aggregates SET total_score = %(new_total_score)s,updated_at=NOW() "
                                    "WHERE osm_id = %(osm_id)s AND campaign_name=%(campaign_name)s")
                
                    params = {"new_total_score":new_total_score,"osm_id":resultset['osm_id'],'campaign_name':resultset['campaign_name']}
                    
                    cursor.execute(update_query,params)
                    connection.commit()

                else:
                    score = (aggregates['total_create_count']*3)+(aggregates['total_modify_count']*1)+(aggregates['total_delete_count']*3)
                    new_entry = ("INSERT INTO leaderboard_aggregates(campaign_name,osm_id,display_name,total_score) "
                                 "VALUES(%(campaign_name)s,%(osm_id)s,%(display_name)s,%(total_score)s)")
                    params = {'campaign_name':aggregates['campaign_name'],
                              "osm_id":aggregates['osm_id'],
                              "display_name":aggregates['display_name'],
                              "total_score":score}
                    cursor.execute(new_entry,params)
                    connection.commit()
            #add last inserted changeset id to the tracker tbl
            new_tracker = "INSERT INTO leaderboard_aggregate_trackers(changeset_id) VALUES(%(changeset_id)s)"
            params = {"changeset_id":last_inserted_chageset_id}
            cursor.execute(new_tracker,params)
            connection.commit()
            print("aggregate table has been updated")
        else:
            print("no new changeset found")
    #No changeset id in the tracker tbl
    else:
        users_changeset_aggregate = ("SELECT osm_id,display_name,campaign_name, "
                                     "sum(create_count) as total_create_count, "
                                     "sum(modify_count) as total_modify_count, "  
                                     "sum(delete_count) as total_delete_count "
                                     "FROM osm_changeset_details "
                                     "WHERE campaign_name = %(campaign_name)s"
                                     "GROUP BY osm_id,display_name,campaign_name")
        params = {"campaign_name":CAMPAIGN_NAME}
        cursor.execute(users_changeset_aggregate,params)
        users_aggregate_resultset = cursor.fetchall()
        for aggregates in users_aggregate_resultset:
            score = (aggregates['total_create_count']*3)+(aggregates['total_modify_count']*1)+(aggregates['total_delete_count']*3)
            new_entry = ("INSERT INTO leaderboard_aggregates(campaign_name,osm_id,display_name,total_score) "
                         "VALUES(%(campaign_name)s,%(osm_id)s,%(display_name)s,%(total_score)s)")
            params = {'campaign_name':aggregates['campaign_name'],
                      'osm_id':aggregates['osm_id'],
                      'display_name':aggregates['display_name'],
                      'total_score':score}
            cursor.execute(new_entry,params)
            connection.commit()
        #add last changeset id to the tracker tbl
        last_changeset_query = ("SELECT changeset_detail_id "
                                "FROM osm_changeset_details "
                                "ORDER BY changeset_detail_id DESC LIMIT 1")
        cursor.execute(last_changeset_query)
        last_changeset_query_result = cursor.fetchone()
        last_chageset_id = last_changeset_query_result['changeset_detail_id']

        new_tracker_id = "INSERT INTO leaderboard_aggregate_trackers(changeset_id) VALUES(%(changeset_id)s)"
        params = {"changeset_id":last_chageset_id}
        cursor.execute(new_tracker_id,params)
        connection.commit()
        print("Added to aggregate table")


    # writing the current timestamp to the text file
    f_write = open("/var/osm_changeset/osm_changeset_timestamp.txt", "w")
    #write the utc datetime to the file
    UTC_DATETIME = datetime.strftime(datetime.now() - timedelta(hours=5,minutes=30), '%Y-%m-%d %H:%M:%S')
    f_write.write(UTC_DATETIME)
    f_write.close()

#close the communication with the database server
cursor.close()
connection.close()
    
